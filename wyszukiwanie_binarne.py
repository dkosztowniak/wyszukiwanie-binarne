'''Wyszukiwanie binarne
Program losuje listę elementów, wyszukuje pozycję wybranego elementu 
na posortowanej liście.
Elementy listy są numerowane od zera.

Materiał dydaktyczny, poziom szkoły podstawowej. 
Python 3'''
# Daria Kosztowniak

import random

def losujLiczby():
    '''Funkcja losująca 100 unikalnych liczb w zakresie od 1 do 999'''
    global mojeLiczby
    mojeLiczby = random.sample(range(1, 1000), k = 100)
    mojeLiczby.sort()
    
def wyswietlZbior():
    '''Funkcja wyświetla zbiór liczb.'''
    print('----------')
    for i in range(100):
        if i % 10 == 0:
            print('')
        print(mojeLiczby[i], '\t', end = '')
    print('\nUporządkowany zbiór liczb zawiera 100 elementów.')

def wyszukaj(lista, szukana):
    '''Wyszykiwanie binarne - funkcja wyszukuje pozycję
    wybranego elementu na posortowanej liście.'''
    a = 0
    b = len(lista)
    global iloscSprawdzen
    iloscSprawdzen = 0
    
    while a < b:
        iloscSprawdzen += 1
        srodek = (a + b) // 2
        if lista[srodek] == szukana:
            return srodek
        else:
            if lista[srodek] < szukana:
                a = srodek + 1
            else:
                b = srodek
    return -1 # gdy element nie został znaleziony

# Losowanie i sortowanie liczb
losujLiczby()

# Wyświetlenie zbioru wylosowanych i posortowanych liczb
wyswietlZbior()

# Blok główny programu
while True:
    print('----------')
    liczbaSprawdzen = 0
    szukanaLiczba = 0
    
    print('k - koniec programu, l - losuje nowe liczby, w - wyświetla aktualny zbiór liczb')
    szukanaLiczba = input('Wprowadź szukaną liczbę większą od zera: ')
    if szukanaLiczba.lower() == 'k':
        print('----------')
        print('Użytkowanik zakończył działanie programu.')
        break
    elif szukanaLiczba.lower() == 'l':
        losujLiczby()
        wyswietlZbior()
    elif szukanaLiczba.lower() == 'w':
        wyswietlZbior()
    
    try:
        szukanaLiczba = int(szukanaLiczba)
        if szukanaLiczba <= 0: continue
    except:
        continue
        
    pozycja = wyszukaj(mojeLiczby, szukanaLiczba)

    print('Szukana liczba:', szukanaLiczba, end=' ')
    if pozycja >= 0:
        print('znajduje się na pozycji: ', pozycja)
    else:
        print('nie została odnaleziona na liście.')
    print('Ilość sprawdzeń:', iloscSprawdzen)





