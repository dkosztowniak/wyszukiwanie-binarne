# Wyszukiwanie binarne
Implementacja algorytmu wyszukiwania binarnego.  
Materiał dydaktyczny, poziom szkoły podstawowej.

Program losuje listę składającą się ze 100 elementów (liczb naturalnych w zakresie od 1 do 999), 
sortuje listę, następnie wyszukuje i wyświetla pozycję wybranego elementu na liście.  
Elementy listy są numerowane od zera.

[Sprawdź jak to działa »](https://onlinegdb.com/w0lDs_YqQ)
